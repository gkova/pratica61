
import java.util.Map;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        
        Jogador jogador11 = new Jogador(1, "Fulano");
        Jogador jogador12 = new Jogador(4, "Ciclano");
        Jogador jogador13 = new Jogador(10, "Beltrano");
        
        Jogador jogador21 = new Jogador(1, "João");
        Jogador jogador22 = new Jogador(7, "José");
        Jogador jogador23 = new Jogador(15, "Mário");
        
        time1.addJogador("Goleiro", jogador11);
        time1.addJogador("Lateral", jogador12);
        time1.addJogador("Atacante", jogador13);
        
        time2.addJogador("Goleiro", jogador21);
        time2.addJogador("Lateral", jogador22);
        time2.addJogador("Atacante", jogador23);
        
 
    System.out.println("Posição         Time1            Time2");
    
    for (String key1 : time1.getJogadores().keySet()) {	//Capturamos o valor a partir da chave
        Jogador value1 = time1.getJogadores().get(key1);
        for (String key2 : time2.getJogadores().keySet()){
            Jogador value2 = time2.getJogadores().get(key2);
            if (key1==key2){
                System.out.println(key1 + "     " + time1.getJogadores().get(key1) + "     " + time2.getJogadores().get(key1));
            }
        }
    }
    }
}
