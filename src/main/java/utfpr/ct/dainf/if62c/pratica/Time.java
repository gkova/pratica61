/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;

/**
 *
 * @author gabriel
 */
public class Time {
    private HashMap<String, Jogador> jogadores = new HashMap<>();

    /**
     *
     * @return
     */
    public HashMap<String, Jogador> getJogadores(){
        return jogadores;
    }
    
    /**
     *
     * @param posicao
     * @param jogador
     */
    public void addJogador(String posicao, Jogador jogador){
        jogadores.put(posicao, jogador);
    }
    /*
    public static void main(String[] args) {
    
        
    time.put("Goleiro", new Jogador(12, "Júlio César"));
    time.put("Zagueiro", new Jogador(4, "David Luiz"));
    time.put("Atacante", new Jogador(10, "Neymar"));
    
    System.out.println("Escalação:");
    System.out.println(time.get("Goleiro"));
    System.out.println(time.get("Zagueiro"));
    System.out.println(time.get("Atacante"));
}
*/
}
